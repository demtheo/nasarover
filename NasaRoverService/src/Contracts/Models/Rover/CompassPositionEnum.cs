namespace NasaRoverService.Contracts
{
  public enum CompassPositionEnum
  {
    N,
    E,
    S,
    W,
  }
}
