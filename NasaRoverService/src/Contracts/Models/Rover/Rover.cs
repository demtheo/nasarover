namespace NasaRoverService.Contracts
{
  public class Rover
  {
    public int Xposition { get; set; }
    public int Yposition { get; set; }
    public CompassPositionEnum FacingPosition { get; set; }
  }
}