namespace NasaRoverService.Contracts
{
  public class GridObstacle
  {
    public int Xposition { get; set; }
    public int Yposition { get; set; }
  }
}