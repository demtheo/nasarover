using System.ComponentModel.DataAnnotations;

namespace NasaRoverService.Contracts
{
  public class MoveRoverInput
  {
    [Required]
    [RegularExpression("^[FBLR]*$", ErrorMessage = "Please provide one or more of the available commands: F, B, L, R")]
    public string Command { get; set; }
  }
}