﻿using Microsoft.AspNetCore.Mvc;
using NasaRoverService.Contracts;
using NasaRoverService.Services;

namespace NasaRoverService.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class RoverController : ControllerBase
    {
        private readonly IMoveRoverService roverService;

        public RoverController(IMoveRoverService roverService)
        {
            this.roverService = roverService;
        }

        [HttpPost]
        public Rover Move(MoveRoverInput input)
        {
            return roverService.MoveToPosition(input);
        }
    }
}
