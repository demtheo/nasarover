using NasaRoverService.Contracts;

namespace NasaRoverService.Services
{
  public interface IRoverService
  {
    Rover RotateRight();
    Rover RotateLeft();
    Rover MoveForward();
    Rover MoveBackward();
  }
}