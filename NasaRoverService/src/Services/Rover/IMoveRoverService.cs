using NasaRoverService.Contracts;

namespace NasaRoverService.Services
{
  public interface IMoveRoverService
  {
    Rover MoveToPosition(MoveRoverInput input);
  }
}