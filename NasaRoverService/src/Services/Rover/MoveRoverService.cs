using System;
using NasaRoverService.Contracts;

namespace NasaRoverService.Services
{
  public class MoveRoverService : IMoveRoverService
  {
    private readonly IRoverService roverService;
    public MoveRoverService(IRoverService roverService)
    {
      this.roverService = roverService;
    }
    public Rover MoveToPosition(MoveRoverInput input)
    {
      var rover = new Rover();
      var commands = input.Command.ToCharArray();
      foreach (var command in commands) {
				switch(command)
				{
					case 'F':
						rover = roverService.MoveForward();
						break;
					case 'B':
						rover = roverService.MoveBackward();
						break;
					case 'L':
						rover = roverService.RotateLeft();
						break;
					case 'R':
						rover = roverService.RotateRight();
						break;
					default:
						throw new InvalidOperationException("Unknown input command");
				}
			}
			return rover;
		}
  }
}