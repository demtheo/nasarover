using System;
using NasaRoverService.Contracts;

namespace NasaRoverService.Services
{
  public class RoverService : IRoverService
  {
    private Rover rover;
    private readonly IPlanetGridService planetGridService;
    public RoverService(IPlanetGridService planetGridService)
    {
        this.planetGridService = planetGridService;
        rover = new Rover {
            Xposition = 1,
            Yposition = 1,
            FacingPosition = CompassPositionEnum.N,
        };
    }
    private bool IsOnBackGridSide() {
      return rover.Xposition < 1 || rover.Yposition < 1;
    }
    private bool IsOnTheTopEdge() {
      var planetGrid = this.planetGridService.Grid;
      return rover.Yposition == planetGrid.Yrectagles || rover.Yposition == -planetGrid.Yrectagles;
    }
    private bool IsOnTheBottomEdge() {
      return rover.Yposition == 1 || rover.Yposition == -1;
    }
    private bool IsOnTheRightEdge() {
      var planetGrid = this.planetGridService.Grid;
      return rover.Xposition == planetGrid.Xrectagles || rover.Xposition == -planetGrid.Xrectagles;
    }
    private bool IsOnTheLeftEdge() {
      return rover.Xposition == 1 || rover.Xposition == -1;
    }
    public Rover RotateRight() {
      switch (rover.FacingPosition)
      {
        case (CompassPositionEnum.N):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.W : CompassPositionEnum.E;
          return rover;
        case (CompassPositionEnum.E):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.N : CompassPositionEnum.S;
          return rover;
        case (CompassPositionEnum.S):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.E : CompassPositionEnum.W;
          return rover;
        case (CompassPositionEnum.W):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.S : CompassPositionEnum.N;
          return rover;
        default:
          throw new InvalidOperationException("The rover's facing position is invalid");
      }
    }
    public Rover RotateLeft() {
      switch (rover.FacingPosition)
      {
        case (CompassPositionEnum.N):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.E : CompassPositionEnum.W;
          return rover;
        case (CompassPositionEnum.E):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.S : CompassPositionEnum.N;
          return rover;
        case (CompassPositionEnum.S):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.W : CompassPositionEnum.E;
          return rover;
        case (CompassPositionEnum.W):
          rover.FacingPosition = IsOnBackGridSide() ? CompassPositionEnum.N : CompassPositionEnum.S;
          return rover;
        default:
          throw new InvalidOperationException("The rover's facing position is invalid");
      }
    }
    public Rover MoveForward() {
      var planetGrid = this.planetGridService.Grid;
      switch (rover.FacingPosition)
      {
        case CompassPositionEnum.N:
          if (IsOnTheTopEdge()) {
            rover.Yposition = rover.Yposition < 1 ? planetGrid.Yrectagles : -planetGrid.Yrectagles;
            rover.Xposition = rover.Yposition < 1 ? rover.Xposition * -1 : rover.Xposition / -1;
            rover.FacingPosition = CompassPositionEnum.S;
          } else {
            rover.Yposition = rover.Yposition < 1 ?  rover.Yposition - 1 : rover.Yposition + 1;
          }
          return rover;
        case CompassPositionEnum.S:
          if (IsOnTheBottomEdge()) {
            rover.Yposition = rover.Yposition < 1 ? +1 : -1;
            rover.Xposition = rover.Xposition < 1 ? rover.Xposition * -1 : rover.Xposition / -1;
            rover.FacingPosition = CompassPositionEnum.N;
          } else {
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition + 1 : rover.Yposition - 1;
          }
          return rover;
        case CompassPositionEnum.E:
          if (IsOnTheRightEdge()) {
            rover.Xposition = rover.Xposition < 1 ? planetGrid.Xrectagles : -planetGrid.Xrectagles;
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition * -1 : rover.Yposition / -1;
            rover.FacingPosition = CompassPositionEnum.W;
          } else {
            rover.Xposition = rover.Xposition < 1 ? rover.Xposition - 1 : rover.Xposition + 1;
          }
          return rover;
        case CompassPositionEnum.W:
          if(IsOnTheLeftEdge()) {
            rover.Xposition = rover.Xposition < 1 ? +1 : -1;
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition * -1 : rover.Yposition / -1;
            rover.FacingPosition = CompassPositionEnum.E;
          } else {
            rover.Xposition = rover.Xposition < 1 ? rover.Xposition + 1 : rover.Xposition - 1;
          }
          return rover;
        default:
          throw new InvalidOperationException("The rover's facing position is invalid");
      }
    }
    public Rover MoveBackward() {
      var planetGrid = this.planetGridService.Grid;
      switch (rover.FacingPosition)
      {
        case (CompassPositionEnum.N):
          if (IsOnTheBottomEdge()) {
            rover.Yposition = rover.Yposition < 1 ? +1 : -1;
            rover.Xposition = rover.Xposition < 1 ? rover.Xposition * -1 : rover.Xposition / -1;
            rover.FacingPosition = CompassPositionEnum.S;
          } else {
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition + 1 : rover.Yposition - 1;
          }
          return rover;
        case (CompassPositionEnum.S):
          if (IsOnTheTopEdge()) {
            rover.Yposition = rover.Yposition < 1 ? planetGrid.Yrectagles : -planetGrid.Yrectagles;
            rover.Xposition = rover.Yposition < 1 ? rover.Xposition * -1 : rover.Xposition / -1;
            rover.FacingPosition = CompassPositionEnum.N;
          } else {
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition - 1 : rover.Yposition + 1;
          }
          return rover;
        case (CompassPositionEnum.E):
          if (IsOnTheLeftEdge()) {
            rover.Xposition = rover.Xposition < 1 ? +1 : -1;
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition * -1 : rover.Yposition / -1;
            rover.FacingPosition = CompassPositionEnum.W;
          } else {
            rover.Xposition = rover.Xposition < 1 ? rover.Xposition + 1 : rover.Xposition - 1;
          }
          return rover;
        case (CompassPositionEnum.W):
          if (IsOnTheRightEdge()) {
            rover.Xposition = rover.Xposition < 1 ? planetGrid.Xrectagles : -planetGrid.Xrectagles;
            rover.Yposition = rover.Yposition < 1 ? rover.Yposition * -1 : rover.Yposition / -1;
            rover.FacingPosition = CompassPositionEnum.E;
          } else {
            rover.Xposition = rover.Xposition < 1 ? rover.Xposition - 1 : rover.Xposition + 1;
          }
          return rover;
        default:
          throw new InvalidOperationException("The rover's facing position is invalid");
      }
    }
    
  }
}