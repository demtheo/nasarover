using System.Collections.Generic;
using NasaRoverService.Contracts;

namespace NasaRoverService.Services
{
  public class PlanetGridService : IPlanetGridService
  {
    public PlanetGrid Grid { get; private set; }

    public PlanetGridService()
    {
      Grid = new PlanetGrid {
          Xrectagles = 6,
          Yrectagles = 6,
      };
    }
  }
}