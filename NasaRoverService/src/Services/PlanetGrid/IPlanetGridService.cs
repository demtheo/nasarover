using NasaRoverService.Contracts;

namespace NasaRoverService.Services
{
  public interface IPlanetGridService
  {
    PlanetGrid Grid { get; }
  }
}