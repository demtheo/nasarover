# Nasa Rover Service #

### How to start this service: ###

* cd ./NasaRoverService
* dotnet run

### How to run tests: ###

* cd ./NasaRoverService.Tests 
* dotnet test

### Implementation logic ###

* Grid is set to 6x*6y
* Rover's starting position is (x=1, y=1, N)
* The cardinal compass points are based on the image https://www.geographyrealm.com/wp-content/uploads/2013/07/compass-ross-cardinal-points.png
* For grid wrapping implementation we use negative numbers to reperesent the other side of the sphere eg (x=-4, y=-6, S)
* Rotation logic in the other side of the sphere see tests in file RoverRotateFromTheBackSideOfGridTests.cs

