using Xunit;
using NasaRoverService.Controllers;
using NasaRoverService.Contracts;
using NasaRoverService.Services;

namespace NasaRoverService.Tests
{
    public class RoverControllerTests
    {
        private IMoveRoverService moveRoverService;
        private IRoverService roverService;
        private IPlanetGridService planetGridService;
        public RoverControllerTests() {
            planetGridService = new PlanetGridService();
            roverService = new RoverService(planetGridService);
            moveRoverService = new MoveRoverService(roverService);
        }

        [Fact]
        public void ExecuteMoveEndpoint()
        {
            var input = new MoveRoverInput {Command= "F"};
            var controller = new RoverController(moveRoverService);
            
            var rover = controller.Move(input);

            Assert.NotNull(rover);
        }

    }
}
