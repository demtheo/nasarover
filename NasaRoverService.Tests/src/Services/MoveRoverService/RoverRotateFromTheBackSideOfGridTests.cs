using Xunit;
using NasaRoverService.Contracts;
using NasaRoverService.Services;

namespace NasaRoverService.Tests
{
    public class RoverRotateFromTheBackSideOfGridTests
    {
        private IMoveRoverService moveRoverService;
        private IRoverService roverService;
        private IPlanetGridService planetGridService;
        public RoverRotateFromTheBackSideOfGridTests() {
            planetGridService = new PlanetGridService();
            roverService = new RoverService(planetGridService);
            moveRoverService = new MoveRoverService(roverService);
        }

        [Fact]
        public void IfRotateRight_CanFaceWestFromNorthCardinalPosition()
        {
            var setToNegativeNorthPositionInput = new MoveRoverInput {Command= "RRF"};
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeNorthPositionInput);
            Assert.Equal(CompassPositionEnum.N, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceEastFromNorthCardinalPosition()
        {
            var setToNegativeNorthPositionInput = new MoveRoverInput {Command= "RRF"};
            var input = new MoveRoverInput {Command= "L"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeNorthPositionInput);
            Assert.Equal(CompassPositionEnum.N, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateRight_CanFaceNorthFromEastCardinalPosition()
        {
            var setToNegativeEastPositionInput = new MoveRoverInput {Command= "RRFL"};
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeEastPositionInput);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.N, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceSouthFromEastCardinalPosition()
        {
            var setToNegativeEastPositionInput = new MoveRoverInput {Command= "RRFL"};
            var input = new MoveRoverInput {Command= "L"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeEastPositionInput);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateRight_CanFaceEastFromSouthCardinalPosition()
        {
            var setToNegativeSouthPositionInput = new MoveRoverInput {Command= "B"};
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeSouthPositionInput);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceWestFromSouthCardinalPosition()
        {
            var setToNegativeSouthPositionInput = new MoveRoverInput {Command= "B"};
            var input = new MoveRoverInput {Command= "L"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeSouthPositionInput);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateRight_CanFaceSouthFromWestCardinalPosition()
        {
            var setToNegativeWestPositionInput = new MoveRoverInput {Command= "BL"};
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeWestPositionInput);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceNorthFromWestCardinalPosition()
        {
            var setToNegativeWestPositionInput = new MoveRoverInput {Command= "BL"};
            var input = new MoveRoverInput {Command= "L"};
            
            var rover = moveRoverService.MoveToPosition(setToNegativeWestPositionInput);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
            Assert.Equal(-1, rover.Yposition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.N, rover.FacingPosition);
        }

    }
}
