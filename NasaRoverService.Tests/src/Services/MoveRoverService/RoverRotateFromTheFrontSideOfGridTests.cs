using Xunit;
using NasaRoverService.Contracts;
using NasaRoverService.Services;

namespace NasaRoverService.Tests
{
    public class RoverRotateFromTheFrontSideOfGridTests
    {
        private IMoveRoverService moveRoverService;
        private IRoverService roverService;
        private IPlanetGridService planetGridService;
        public RoverRotateFromTheFrontSideOfGridTests() {
            planetGridService = new PlanetGridService();
            roverService = new RoverService(planetGridService);
            moveRoverService = new MoveRoverService(roverService);
        }

        [Fact]
        public void IfRotateRight_CanFaceEastFromNorthCardinalPosition()
        {
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(input);

            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceWestFromNorthCardinalPosition()
        {
            var input = new MoveRoverInput {Command= "L"};
            
            var rover = moveRoverService.MoveToPosition(input);

            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateRight_CanFaceSouthFromEastCardinalPosition()
        {
            var input = new MoveRoverInput {Command= "R"};

            var rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceNorthFromEastCardinalPosition()
        {
            var setToEastPositionInput = new MoveRoverInput {Command= "R"};
            var input = new MoveRoverInput {Command= "L"};

            var rover = moveRoverService.MoveToPosition(setToEastPositionInput);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.N, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateRight_CanFaceWestFromSouthCardinalPosition()
        {
            var setToSouthPositionInput = new MoveRoverInput {Command= "RR"};
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(setToSouthPositionInput);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceEastFromSouthCardinalPosition()
        {
            var setToSouthPositionInput = new MoveRoverInput {Command= "RR"};
            var input = new MoveRoverInput {Command= "L"};

            var rover = moveRoverService.MoveToPosition(setToSouthPositionInput);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.E, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateRight_CanFaceNorthFromWestCardinalPosition()
        {
            var setToWestPositionInput = new MoveRoverInput {Command= "RRR"};
            var input = new MoveRoverInput {Command= "R"};
            
            var rover = moveRoverService.MoveToPosition(setToWestPositionInput);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.N, rover.FacingPosition);
        }

        [Fact]
        public void IfRotateLeft_CanFaceSouthFromWestCardinalPosition()
        {
            var setToWestPositionInput = new MoveRoverInput {Command= "RRR"};
            var input = new MoveRoverInput {Command= "L"};

            var rover = moveRoverService.MoveToPosition(setToWestPositionInput);
            Assert.Equal(CompassPositionEnum.W, rover.FacingPosition);
            rover = moveRoverService.MoveToPosition(input);
            Assert.Equal(CompassPositionEnum.S, rover.FacingPosition);
        }

    }
}
