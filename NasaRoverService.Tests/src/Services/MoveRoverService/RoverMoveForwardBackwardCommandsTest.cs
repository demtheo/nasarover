using Xunit;
using NasaRoverService.Controllers;
using NasaRoverService.Contracts;
using NasaRoverService.Services;

namespace NasaRoverService.Tests
{
    public class RoverMoveForwardBackwardCommandsTest
    {
        private IMoveRoverService moveRoverService;
        private IRoverService roverService;
        private IPlanetGridService planetGridService;
        public RoverMoveForwardBackwardCommandsTest() {
            planetGridService = new PlanetGridService();
            roverService = new RoverService(planetGridService);
            moveRoverService = new MoveRoverService(roverService);
        }

        [Fact]
        public void MoveForwardWithSingleCommand()
        {
            var input = new MoveRoverInput {Command= "F"};
            
            var rover = moveRoverService.MoveToPosition(input);

            Assert.NotNull(rover);
            Assert.Equal(2, rover.Yposition);
        }

        [Fact]
        public void MoveBackwardWithSingleCommand()
        {
            var input = new MoveRoverInput {Command= "B"};
            
            var rover = moveRoverService.MoveToPosition(input);

            Assert.Equal(-1, rover.Yposition);
        }

        [Fact]
        public void MoveForwardWithMultipleCommands()
        {
            var input = new MoveRoverInput {Command= "FF"};
            
            var rover = moveRoverService.MoveToPosition(input);

            Assert.Equal(3, rover.Yposition);
        }

        [Fact]
        public void MoveBackwardMultipleCommands()
        {
            var input = new MoveRoverInput {Command= "BB"};
            
            var rover = moveRoverService.MoveToPosition(input);
            
            Assert.Equal(-2, rover.Yposition);
        }

    }
}
